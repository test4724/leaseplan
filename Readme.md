**How to install, run and write tests**

_Tests were written using:_
- SerenityBDD https://serenity-bdd.github.io/theserenitybook/latest/index.html 
- RestAssured https://rest-assured.io/ 
- Cucumber https://cucumber.io/docs/tools/java/ 
- Junit https://junit.org/junit5/ 
- Gradle https://gradle.org/

_Install_
1. Need to be installed (follow instructions here https://digital.ai/catalyst-blog/working-with-serenity-bdd-framework-an-overview):
- Java
- IntelliJ IDE
- Gradle
- Serenity BDD Plugin

2. Open project's "build.gradle" file and modify yours with provided data

_Run tests_
1. Open feature file and click on green icon near scenario's title \ feature's title OR
2. In Intellij IDEA open Gradle window (View\Tools window\Gradle) and run Gradle's task "Cucumber" OR
3. From command line: gradle cucumber
4. To view reports open ../leaseplan/target/leaseplan-test-report.html

_How to write new tests_
1. Follow instructions here https://www.jetbrains.com/help/idea/cucumber-support.html 

_Some of best practices for creating api tests:_
1. Create separate classes for: Helpers, Constants, Environment variables, Models, CommonAsserts etc
2. Create different Feature files for each api
3. Use Steps from One feature file in Other’s feature file (to avoid duplicate code) if needed
4. Use Context if you need to pass data between feature files (for example, pass Id)
5. Create Hooks for doing some action before\after tests (like delete data after tests, cleanup)
6. Tests for unauthorized user could be added to separate feature file
7. Provide different scenarios: positive \ negative \ border cases \ invalid data etc
8. Try to call endpoint without mandatory data
9. If response code is 200 (OK) then parse response’s data, if not - provide response error 








