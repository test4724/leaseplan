Feature: New York Times information about book reviews and bestsellers lists

  Scenario: Authorized user is able to see best-sellers list overview without specified publishedDate
    Given As an authorized user I want to see list view
    Then List overview is retrieved

  Scenario: Authorized user is able to see best-sellers list overview with specified publishedDate
    Given As an authorized user I want to see list overview with specified publishedDate
    Then List overview for the closest publication date is retrieved

  Scenario: Try to get best-sellers list overview with specified incorrect publishedDate
    #incorrect publishedDate could be: incorrect format or not existed date

  Scenario: Unauthorized user is not able to see best-sellers list overview
    Given As an unauthorized user I try to retrieve best-sellers list overview
    Then StatusCode is Unauthorized

  Scenario: User with invalid credentials is not able to see best-sellers list overview
    Given As an user with invalid credentials I try to retrieve best-sellers list overview
    Then StatusCode is Unauthorized

  Scenario: Get lists names
    Given As an authorized user I get lists names
    Then Bestsellers list is retrieved

  #Due to time constraints assume that this endpoint contains only 1 available field "list"
  #Let's assume that for testing purposes, ListName we created before and we now list's name already
  Scenario: Get list by list name
    Given As an authorized user I get lists names
    When I try to get data for specific list "Combined-Print-and-E-Book-Fiction"
    Then List with ranks for specific bestseller list is retrieved

  Scenario Outline: Get list by NotExistedListName name
    Given As an authorized user I get lists names
    When I try to get data for specific list "<ListName>"
    Then Retrieved list is empty
    Examples:
    |ListName|
    |  NotExistedListName      |
    #|  SomeMinNumberOfCharacters  |   need requirement specification
    #|  SomeMaxNumberOfCharacters  |   need requirement specification

  Scenario Outline: Get list by invalid list name
    Given As an authorized user I get lists names
    When I try to get data for specific list "<ListName>"
    Then StatusCode is BadRequest
    Examples:
    |  ListName                   |
    |  null                       |
    #|  EmptyListName              | returns StatusCode = 200, It is ok if this is requirement
    #|  NotAllowedCharacters       |   need requirement specification



