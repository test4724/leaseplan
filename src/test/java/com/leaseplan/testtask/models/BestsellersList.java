package com.leaseplan.testtask.models;

public class BestsellersList
{
    private String listName;
    private String oldestPublishedDate;
    private String newestPublishedDate;

    public BestsellersList(String listName)
    {
        this.listName = listName;
    }

    public BestsellersList(String listName, String oldestPublishedDate, String newestPublishedDate) {
        this.listName = listName;
        this.oldestPublishedDate = oldestPublishedDate;
        this.newestPublishedDate = newestPublishedDate;
    }

    public String getListName() {
        return listName;
    }

    public String getOldestPublishedDate() {
        return oldestPublishedDate;
    }

    public String getNewestPublishedDate() {
        return newestPublishedDate;
    }
}
