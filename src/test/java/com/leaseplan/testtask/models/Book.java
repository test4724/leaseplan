package com.leaseplan.testtask.models;

public class Book
{
     private String author;
     private String title;
     private String price;
     private String createdDate;

     public Book(String author, String title) {
          this.author = author;
          this.title = title;
     }

     public Book(String author, String title, String price, String createdDate) {
          this.author = author;
          this.title = title;
          this.price = price;
          this.createdDate = createdDate;
     }

     public Book(String author, String title, String createdDate) {
          this.author = author;
          this.title = title;
          this.createdDate = createdDate;
     }

     public void setPrice(String price) {
          this.price = price;
     }

     public void setCreatedDate(String createdDate) {
          this.createdDate = createdDate;
     }

     public String getAuthor() {
          return author;
     }

     public String getTitle() {
          return title;
     }

     public String getPrice() {
          return price;
     }

     public String getCreatedDate() {
          return createdDate;
     }
}
