package com.leaseplan.testtask.models;

public class SpecifiedListName
{
    private String specifiedListName;
    private int rank;
    private int rankLastWeek;

    public SpecifiedListName(String specifiedListName) {
        this.specifiedListName = specifiedListName;
    }

    public SpecifiedListName(String specifiedListName, int rank, int rankLastWeek) {
        this.specifiedListName = specifiedListName;
        this.rank = rank;
        this.rankLastWeek = rankLastWeek;
    }

    public String getSpecifiedListName() {
        return specifiedListName;
    }

    public int getRank() {
        return rank;
    }

    public int getRankLastWeek() {
        return rankLastWeek;
    }

    public void setSpecifiedListName(String specifiedListName) {
        this.specifiedListName = specifiedListName;
    }
}
