package com.leaseplan.testtask.ApiControllers;

import com.leaseplan.testtask.models.BestsellersList;
import com.leaseplan.testtask.models.Book;
import com.leaseplan.testtask.models.SpecifiedListName;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import org.apache.http.HttpStatus;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class NYTController
{
    private final String BASE_URI = "https://api.nytimes.com/";
    private final String API_KEY = "WZescbMQmy1us6txVIlfNlXZgYKRJ5eB";
    private final String BESTSELLERSLIST_ENDPOINT = "svc/books/v3/lists/overview.json?api-key=";
    private final String LISTNAMES_ENDPOINT = "svc/books/v3/lists/names.json?api-key=";
    private final String LISTS_ENDPOINT = "svc/books/v3/lists.json?api-key=";

    private Response response;
    private String sessionApiKey;

    public void authorizeWithDefaultApiKey()
    {
        sessionApiKey = API_KEY;
    }

    public void authorizeWithApiKey(String apiKey)
    {
        sessionApiKey = apiKey;
    }

    public void unauthorize()
    {
        sessionApiKey = "";
    }

    public int getStatusCode()
    {
        return response != null ? response.getStatusCode() : 0;
    }

    public ArrayList<Book> getBestsellersList()
    {
        ArrayList<Book> books = new ArrayList<>();

        response = SerenityRest.get(BASE_URI + BESTSELLERSLIST_ENDPOINT + sessionApiKey);
        if (response.getStatusCode() == HttpStatus.SC_OK) {
            List<List<LinkedHashMap<String, String>>> booksList = response.body().jsonPath().getList("results.lists.books");

            for (List<LinkedHashMap<String, String>> list : booksList) {
                for (LinkedHashMap<String, String> book : list) {
                    books.add(new Book(book.get("author"), book.get("title"), book.get("price"), book.get("created_date")));
                }
            }
        }

        return books;
    }

    public ArrayList<Book> getBestsellersListWithSpecifiedPublishedDate(LocalDate publishedDate)
    {
        ArrayList<Book> books = new ArrayList<>();

        response = SerenityRest.get(BASE_URI + BESTSELLERSLIST_ENDPOINT + sessionApiKey + "&published_date=" + publishedDate);
        List<List<LinkedHashMap<String, String>>> booksList = response.body().jsonPath().getList("results.lists.books");

        for (List<LinkedHashMap<String, String>> list : booksList) {
            for (LinkedHashMap<String, String> book : list) {
                books.add(new Book(book.get("author"), book.get("title"), book.get("price"), book.get("created_date")));
            }
        }

        return books;
    }

    public ArrayList<BestsellersList> getListsNames()
    {
        ArrayList<BestsellersList> listsNames = new ArrayList<>();

        response = SerenityRest.get(BASE_URI + LISTNAMES_ENDPOINT + sessionApiKey);

        List<LinkedHashMap<String, String>> listNames = response.body().jsonPath().getList("results");

        for (LinkedHashMap<String, String> list : listNames) {
            listsNames.add(new BestsellersList(list.get("list_name"), list.get("oldest_published_date"), list.get("newest_published_date")));
        }

        return listsNames;
    }

    public ArrayList<SpecifiedListName> getListsForSpecificListName(String list)
    {
        ArrayList<SpecifiedListName> lists = new ArrayList<>();

        response = SerenityRest.get(BASE_URI + LISTS_ENDPOINT + sessionApiKey + (list == null ? "" : ("&list=" + list)));

        List<LinkedHashMap<String, Object>> listsSpecificName = response.body().jsonPath().getList("results");
        for (LinkedHashMap<String, Object> el : listsSpecificName) {
            String listName = el.get("list_name").toString();
            int rank = (int) el.get("rank");
            int rankLastWeek = (int) el.get("rank_last_week");

            lists.add(new SpecifiedListName(listName, rank, rankLastWeek));
        }

        return lists;
    }

    public LocalDate getPreviousPublishedDate()
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        String previousPublishedDateString = response.body().jsonPath().get("results.previous_published_date");
        return LocalDate.parse(previousPublishedDateString, formatter);
    }

    public LocalDate getClosestPublicationDate()
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        String closestPublicationDateString = response.body().jsonPath().get("results.next_published_date");
        return LocalDate.parse(closestPublicationDateString, formatter);
    }
}
