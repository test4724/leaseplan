package com.leaseplan.testtask;

import com.leaseplan.testtask.ApiControllers.NYTController;
import com.leaseplan.testtask.models.BestsellersList;
import com.leaseplan.testtask.models.Book;
import com.leaseplan.testtask.models.SpecifiedListName;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.junit.runners.SerenityRunner;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.util.ArrayList;

@RunWith(SerenityRunner.class)
public class NYTStepDefinitions
{
    private ArrayList<Book> bookArrayList;
    private ArrayList<BestsellersList> listsNames;
    private ArrayList<SpecifiedListName> listsForSpecificListName;
    NYTController nytController = new NYTController();

    @Given("As an authorized user I want to see list view")
    public void asAnAuthorizedUserIWantToSeeListView()
    {
        nytController.authorizeWithDefaultApiKey();
        bookArrayList = nytController.getBestsellersList();
    }

    @Then("List overview is retrieved")
    public void listOverviewIsRetrieved()
    {
        Assert.assertNotNull("List of books is not empty", bookArrayList);
        for (Book book : bookArrayList)
        {
            Assert.assertNotNull("Author's field is not empty", book.getAuthor());
            Assert.assertNotNull("Title's field is not empty", book.getTitle());
            Assert.assertNotNull("Price's field is not empty", book.getPrice());
            Assert.assertNotNull("CreatedDate's field is not empty", book.getCreatedDate());
        }

        LocalDate previousPublishedDate = nytController.getPreviousPublishedDate();
        LocalDate today = LocalDate.now();
        Assert.assertTrue("Current week's best-sellers lists is returned", previousPublishedDate.isAfter(today.minusDays(6)));
    }

    @Given("As an unauthorized user I try to retrieve best-sellers list overview")
    public void asAnUnauthorizedUserITryToRetrieveBestSellersListOverview()
    {
        ArrayList<Book> bookArrayList = nytController.getBestsellersList();
    }

    @Then("StatusCode is Unauthorized")
    public void statuscodeIsUnauthorized()
    {
        int statusCode = nytController.getStatusCode();
        Assert.assertEquals(HttpStatus.SC_UNAUTHORIZED, statusCode);
    }

    @Given("As an user with invalid credentials I try to retrieve best-sellers list overview")
    public void asAnUserWithInvalidCredentialsITryToRetrieveBestSellersListOverview()
    {
        nytController.authorizeWithApiKey("invalidApiKey");
        ArrayList<Book> bookArrayList = nytController.getBestsellersList();
    }

    @Given("As an authorized user I want to see list overview with specified publishedDate")
    public void asAnAuthorizedUserIWantToSeeListOverviewWithSpecifiedPublishedDate()
    {
        nytController.authorizeWithDefaultApiKey();
        LocalDate publishedDate = LocalDate.now();
        bookArrayList = nytController.getBestsellersListWithSpecifiedPublishedDate(publishedDate);
    }

    @Then("List overview for the closest publication date is retrieved")
    public void listOverviewForTheClosestPublicationDateIsRetrieved()
    {
        Assert.assertNotNull("List of books is not empty", bookArrayList);
        for (Book book : bookArrayList)
        {
            Assert.assertNotNull("Author's field is not empty", book.getAuthor());
            Assert.assertNotNull("Title's field is not empty", book.getTitle());
            Assert.assertNotNull("Price's field is not empty", book.getPrice());
            Assert.assertNotNull("CreatedDate's field is not empty", book.getCreatedDate());
        }

        LocalDate closestPublicationDate = nytController.getClosestPublicationDate();
        LocalDate today = LocalDate.now();
        Assert.assertTrue("List overview for closest publication date is returned", closestPublicationDate.isAfter(today));
    }

    @Then("Bestsellers list is retrieved")
    public void bestsellersListIsRetrieved()
    {
        Assert.assertNotNull("ListsNames is not empty", listsNames);

        for (BestsellersList listName : listsNames)
        {
            Assert.assertNotNull("List's name is not empty", listName.getListName());
            Assert.assertNotNull("Title's field is not empty", listName.getNewestPublishedDate());
            Assert.assertNotNull("Price's field is not empty", listName.getOldestPublishedDate());
        }
    }

    @Given("As an authorized user I get lists names")
    public void asAnAuthorizedUserIGetListsNames()
    {
        nytController.authorizeWithDefaultApiKey();
        listsNames = nytController.getListsNames();
    }

    @When("I try to get data for specific list {string}")
    public void iTryToGetDataForSpecificList(String listName)
    {
        //if we had Post endpoint for creating TestList, we would use it here for testing purposes
        //so assume that first one is that list name which we needed
        //listName = listsNames.get(0).getListName();
        if (listName.equals("EmptyListName"))
        {
            listName = "";
        }

        if (listName.equals("null"))
        {
            listName = null;
        }

        listsForSpecificListName = nytController.getListsForSpecificListName(listName);
    }

    @Then("List with ranks for specific bestseller list is retrieved")
    public void listWithRanksForSpecificBestsellerListIsRetrieved()
    {
        Assert.assertNotNull("ListsForSpecificListName is not empty", listsForSpecificListName);

        for (SpecifiedListName specifiedListName : listsForSpecificListName)
        {
            Assert.assertNotNull("List's name is not empty", specifiedListName.getSpecifiedListName());
            Assert.assertTrue(specifiedListName.getRank() >= 0);
            Assert.assertTrue(specifiedListName.getRankLastWeek() >= 0);
        }
    }

    @Then("StatusCode is BadRequest")
    public void statuscodeIsBadRequest()
    {
        int statusCode = nytController.getStatusCode();
        Assert.assertEquals(HttpStatus.SC_BAD_REQUEST, statusCode);
    }

    @Then("Retrieved list is empty")
    public void retrievedListIsEmpty()
    {
        Assert.assertEquals(0, listsForSpecificListName.size());
    }

    @After("Logout")
    public void logout()
    {
        nytController.unauthorize();
    }
}
